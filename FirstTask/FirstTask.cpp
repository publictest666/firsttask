﻿#include <iostream>
#include <string>

using namespace std;

// Получение строки от пользователя.
string GetUserString()
{
    string user_string;

    cout << "Enter string: ";
    getline(cin, user_string);

    return user_string;
}

// Получение сконвертированной строки пользователя.
string GetConvertedString(string str)
{
    string converted_string = str;

    for (int i = 0; i < str.size(); i++)
    {
        char check_char = str[i];
        bool is_duplicate = false;

        for (int j = 0; j < str.size(); j++)
        {
            if (check_char == str[j] && j != i)
            {
                converted_string[j] = ')';
                is_duplicate = true;
            }

            if (is_duplicate)
                converted_string[i] = ')';
            else
                converted_string[i] = '(';
        }
    }

    return converted_string;
}

int main()
{
    string user_string = GetUserString();
    string converted_string = GetConvertedString(user_string);

    cout << "Start: " << "\"" << user_string << "\"" << "\n";
    cout << "End:   " << "\"" << converted_string << "\"" << "\n";

    system("pause");
}
